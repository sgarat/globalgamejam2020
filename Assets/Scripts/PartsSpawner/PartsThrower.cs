﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class PartsThrower : MonoBehaviour
{
    public GameObject BrokenArmPrefab;
    public GameObject HeadToExplodePrefab;
    IEnumerable<Vector3> spawnPositions;
    List<Vector3> takenPositions;

    void Awake()
    {
       spawnPositions = gameObject.GetComponentsInChildren<Transform>().Where(children => children.tag == "Position").Select(gObject => gObject.position);
       takenPositions = new List<Vector3>();
    }

    public void ThrowArm(Vector3 fromPosition, Material armSkin)
    {
        if (takenPositions.Count == spawnPositions.Count()) 
            takenPositions = new List<Vector3>();
        
        var filteredCollection = spawnPositions.Where(position => !takenPositions.Contains(position));
        var index = Random.Range(0, filteredCollection.Count() - 1);
        var destination = filteredCollection.ElementAt(index);
        takenPositions.Add(destination);
        
        var brokenArm = Instantiate(BrokenArmPrefab, fromPosition, Quaternion.identity);
        brokenArm.GetComponent<BrokenArm>().SetSkin(armSkin);
        brokenArm.GetComponent<BrokenArm>().Move(destination);
    }
    
    public void ExplodeHead(Vector3 origin)
    {
        SoundManager.Instance.PlayEndFightVoice();
        var destination = spawnPositions.ElementAt(Random.Range(0, spawnPositions.Count() - 1));
        var brokenArm = Instantiate(HeadToExplodePrefab, origin, Quaternion.identity);
        brokenArm.GetComponent<HeadExplode>().Move(destination);
    }
    
}
