﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameStatus : MonoBehaviour
{
    public int CurrentPlayers = 2;
    public static UnityEvent Death;
    
    void Start()
    {
        if (Death == null)
            Death = new UnityEvent();

        Death.AddListener(KillPlayer);
    }

    void KillPlayer()
    {
        CurrentPlayers -= 1;
        if (CurrentPlayers == 1) 
            SceneManager.LoadScene("MainScene");
    }
}
