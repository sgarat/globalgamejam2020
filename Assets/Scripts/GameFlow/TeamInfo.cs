using UnityEngine;

public class TeamInfo : MonoBehaviour
{
    public PlayerType PlayerType;
    public Team Team;
}

public enum PlayerType
{
    Robot,
    Assistant
}

public enum Team
{
    one,
    two
}

