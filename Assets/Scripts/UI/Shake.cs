using System.Collections;
using UnityEngine;

namespace DefaultNamespace
{
    public class Shake : MonoBehaviour
    {
        public float ForceX;
        public float ForceY;
        public float Time;

        bool isShaking;
        Vector3 initialPosition;
        
        public void ShakeCamera()
        {
            StartCoroutine(Wait());
        }

        void Awake()
        {
            SoundManager.Instance.PlayHoverSound();
            initialPosition = transform.position;
        }

        void Update()
        {
            if (isShaking)
            {
                var x = Random.Range(-ForceX, ForceX);
                var y = Random.Range(-ForceY, ForceY);
                transform.localPosition += new Vector3(x, y , 0);
            }
        }

        IEnumerator Wait()
        {
            isShaking = true;
            yield return new WaitForSeconds(Time);
            isShaking = false;
            transform.position = initialPosition;
        }
    }
}