﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour
{
    void Update()
    {
        if(CustomInput.AnyTrigger)
            SceneManager.LoadScene("MainScene");
    }
}
