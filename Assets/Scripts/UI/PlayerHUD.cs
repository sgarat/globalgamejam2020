﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PlayerHUD : MonoBehaviour
    {
        const float HealthyPercentage = .8f;
        const float DamagePercentage = .5f;
        const float DangerPercentage = .1f;

        public Image LeftArm;
        public Image RightArm;
        public Image Body;

        public Color Healthy;
        public Color Damaged;
        public Color Danger;
        public Color Destroyed;

        public ArmsHealth LeftArmHealth;
        public ArmsHealth RightArmHealth;
        public BodyHealth BodyHealth;

        void Update()
        {
            LeftArm.color = SetArmColor(LeftArmHealth);
            RightArm.color = SetArmColor(RightArmHealth);
            Body.color = SetBodyColor();
        }

        Color SetBodyColor() =>
            (float) BodyHealth.CurrentHealth / BodyHealth.MaxHealth > HealthyPercentage ? Healthy
            : (float) BodyHealth.CurrentHealth / BodyHealth.MaxHealth > DamagePercentage ? Damaged
            : (float) BodyHealth.CurrentHealth / BodyHealth.MaxHealth > DangerPercentage ? Danger
            : Destroyed;

        Color SetArmColor(ArmsHealth armHealth) =>
            (float) armHealth.CurrentHealth / armHealth.MaxHealth > HealthyPercentage ? Healthy
            : (float) armHealth.CurrentHealth / armHealth.MaxHealth > DamagePercentage ? Damaged
            : (float) armHealth.CurrentHealth / armHealth.MaxHealth > DangerPercentage ? Danger
            : Destroyed;
    }
}
