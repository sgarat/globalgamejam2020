﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    List<ParticleSystem> particles;
    // Start is called before the first frame update
    public bool playOnAwake = false;
    
    void Awake()
    {
        particles = GetComponentsInChildren<ParticleSystem>().ToList();
    }

    void Start()
    {
        if (playOnAwake)
            PlayParticles();
    }

    public void PlayParticles()
    {
        foreach (var particle in particles) particle.Play();
    }
    
    public void StopParticles()
    {
        foreach (var particle in particles) particle.Stop();
    }
}
