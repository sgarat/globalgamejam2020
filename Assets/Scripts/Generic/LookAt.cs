using UnityEngine;

public class LookAt
{
    readonly Transform transform;
    readonly int controlNumber;

    public LookAt(Transform transform, int controlNumber)
    {
        this.transform = transform;
        this.controlNumber = controlNumber;
    }
    
    public void Do(bool isAlive)
    {
        var direction = CustomInput.RightStick(controlNumber);
        if (direction.magnitude > .0f && isAlive)
        {
            var desiredRotation = Quaternion.LookRotation(direction, Vector3.up);
            var angles = Quaternion.Angle(transform.rotation, desiredRotation);
            transform.rotation = Quaternion.Slerp(transform.rotation,desiredRotation, Time.deltaTime * 480/angles);
        }
    }

}