using UnityEngine;

public class Movement
{
    readonly AgentMovement agent;
    readonly int controlNumber;

    public Movement(AgentMovement agent, int controlNumber)
    {
        this.agent = agent;
        this.controlNumber = controlNumber;
    }
    
    public void Move(float speed, bool isPlayerAlive)
    {
        var moveSpeed = isPlayerAlive ? speed : 0;
        
        var direction = CustomInput.LeftStick(controlNumber);
        direction.Normalize();
        var velocity = direction * (moveSpeed * Time.deltaTime);
        agent.Move(velocity);
    }
}