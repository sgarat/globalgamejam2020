﻿using System.Collections;
using UnityEngine;

public class ParticleBehaviour : MonoBehaviour
{
    public float LifeTime;
    
    public void StartTimer() => StartCoroutine(DestroyAfterThreshold());

    IEnumerator DestroyAfterThreshold()
    {
        yield return new WaitForSeconds(LifeTime);
        Destroy(gameObject);
    }
}
