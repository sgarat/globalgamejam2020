using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public static class CustomInput
{
    public static Vector3 LeftStick(int controlNumber) =>
       new Vector3(Gamepad.all[controlNumber - 1].leftStick.x.ReadValue(), 0f, 
           Gamepad.all[controlNumber - 1].leftStick.y.ReadValue());

    public static Vector3 RightStick(int controlNumber) =>
        new Vector3(Gamepad.all[controlNumber - 1].rightStick.x.ReadValue(), 0f,
            Gamepad.all[controlNumber - 1].rightStick.y.ReadValue());

    public static bool LeftTrigger(int controller) => Gamepad.all[controller - 1].leftTrigger.wasPressedThisFrame;
    public static bool RightTrigger(int controller) => Gamepad.all[controller -1].rightTrigger.wasPressedThisFrame;

    public static bool AnyTrigger =>
        Gamepad.all.Any(pad => 
            pad.leftTrigger.wasPressedThisFrame || 
            pad.rightTrigger.wasPressedThisFrame);

}