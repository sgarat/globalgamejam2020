using UnityEngine;
using UnityEngine.Audio;

namespace DefaultNamespace
{
    public class SoundSourceAndMixer : MonoBehaviour
    {
        public AudioClip[] clips;
        public AudioMixerGroup mixer;
        public AudioSource source;

        public void Play()
        {
            source.clip = clips[0];
            source.outputAudioMixerGroup = mixer;
            source.Play();
        }

        public void PlayRandom()
        {
            var index = Random.Range(0, clips.Length - 1);
            source.clip = clips[index];
            source.outputAudioMixerGroup = mixer;
            source.Play();
        }

        public void PlayScheduled(double time)
        {
            source.clip = clips[0];
            source.outputAudioMixerGroup = mixer;
            source.PlayScheduled(time);
        }
        
        public double ClipDuration() => (double)clips[0].samples / clips[0].frequency;

        public bool IsPlayingClip() => source.isPlaying;
    }
}