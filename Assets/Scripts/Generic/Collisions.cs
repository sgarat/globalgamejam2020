﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Collisions : MonoBehaviour
{
    const float RayOffSet = .2f; 
    
    public static GameObject GetObject(Vector3 origin, Vector3 direction, Vector3 offsetDirection, float distance)
    {
        Physics.Raycast(origin, direction, out var rayOne, distance);
        Physics.Raycast(origin + offsetDirection * -RayOffSet, direction, out var rayTwo, distance);
        Physics.Raycast(origin + offsetDirection * (-RayOffSet * 2), direction, out var rayThree, distance);
        Physics.Raycast(origin + offsetDirection * RayOffSet, direction, out var rayFour, distance);
        Physics.Raycast(origin + offsetDirection * (RayOffSet * 2), direction, out var rayFive, distance);

        var rays = new List<RaycastHit> {rayOne, rayTwo, rayThree, rayFour, rayFive};
        
        Debug.DrawRay(origin, direction, Color.red);
        Debug.DrawRay(origin + offsetDirection * -RayOffSet, direction, Color.blue);
        Debug.DrawRay(origin + offsetDirection * RayOffSet, direction, Color.green);
        
        return GameObject(rays);
    }

    static GameObject GameObject(IEnumerable<RaycastHit> rays)
    {
        foreach (var hit in rays)
            return hit.transform != null ? hit.transform.gameObject : null;

        return null;
    }
}
