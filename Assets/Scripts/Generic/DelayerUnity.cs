using System;
using System.Collections;
using UnityEngine;

public class DelayerUnity : MonoBehaviour
{
    bool isCoroutineExecuting;
    
    public void Delay(float time, float outTime, Action before, Action after)
    {
        StartCoroutine(ExecuteAfterTime(time, outTime, before, after));
    }     
    
    IEnumerator ExecuteAfterTime(float time, float outTime, Action before, Action after)
    {
        if(isCoroutineExecuting)
            yield break;
        isCoroutineExecuting = true;
        before();
        yield return new WaitForSeconds(time);
        after();
        yield return new WaitForSeconds(outTime);
        isCoroutineExecuting = false;
    }
}