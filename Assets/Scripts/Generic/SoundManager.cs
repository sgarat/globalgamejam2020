using System;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class SoundManager : MonoBehaviour
{
    public SoundSourceAndMixer[] sounds; 
    
    void Awake()
    {
        if (instance != null && instance != this) 
        {
            Destroy(this.gameObject);
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        var time = AudioSettings.dspTime + 0.5d;
        if(!sounds[0].IsPlayingClip())
            PlayMusicIntro(time);
        time += ClipDuration(0);
        if(!sounds[1].IsPlayingClip())
            PlayMusicLoop(time);
    }
    
    static SoundManager instance;

    public static SoundManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<SoundManager>();
            }

            return instance;
        }
    }

    public void PlayHitSfx()
    {
        sounds[2].source.pitch = Random.Range(0.84f, 1.16f);
        sounds[2].Play();
    }

    public void PlayPickUpSfx() => sounds[3].Play();
    public void PlayVoices() => sounds[4].PlayRandom();
    public void PlayEndFightVoice() => sounds[5].Play();
    public void PlayScrapFall() => sounds[6].Play();
    public void PlayHoverSound()
    {
        sounds[7].source.loop = true;
        sounds[7].Play();
    }
    public void PlayDecapitation() => sounds[8].Play();
    void PlayMusicIntro(double waitTime) => sounds[0].PlayScheduled(waitTime);
    void PlayMusicLoop(double waitTime) => sounds[1].PlayScheduled(waitTime);
    double ClipDuration(int clip) => sounds[clip].ClipDuration();

}