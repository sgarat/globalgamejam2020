﻿using DefaultNamespace;
using UnityEngine;

public class RobotController : MonoBehaviour
{
    public AgentMovement Agent;
    public float Speed;
    public Transform LeftAttackSpawner;
    public Transform RightAttackSpawner;
    public float AttackDistance;
    public int ControlNumber;
    public float AttackDelay;
    public ArmsHealth LeftArmHealth;
    public ArmsHealth RightArmHealth;
    public BodyHealth BodyHealth;
    public DelayerUnity LeftArmDelayer;
    public DelayerUnity RightArmDelayer;
    public Animator RobotAnimator;
    public ParticleBehaviour ParticleBehaviour;
    public Shake ShakeCamera;

    Movement movement;
    LookAt lookAt;

    void Awake()
    {
        movement = new Movement(Agent, ControlNumber);
        lookAt = new LookAt(transform, ControlNumber);
    }
    
    void Update()
    {
        var speed = 
            !LeftArmHealth.IsActive && !RightArmHealth.IsActive ? Speed + 1.2f :
            !LeftArmHealth.IsActive ? Speed + .6f :
            !RightArmHealth.IsActive ? Speed + .6f :
            Speed;
        
        movement.Move(speed, BodyHealth.IsAlive);
        lookAt.Do(BodyHealth.IsAlive);

        if (CustomInput.LeftTrigger(ControlNumber) && LeftArmHealth.IsActive && BodyHealth.IsAlive)
            LeftArmDelayer.Delay(
                AttackDelay,
                .5f,
                () => RobotAnimator.SetTrigger("LeftAttack"),
                () => Attack.Do(LeftAttackSpawner, AttackDistance, () => CreateParticles(), ShakeCamera)
        );
        else if (CustomInput.RightTrigger(ControlNumber) && RightArmHealth.IsActive && BodyHealth.IsAlive)
            RightArmDelayer.Delay(
            AttackDelay,
            .5f,
            () => RobotAnimator.SetTrigger("RightAttack"),
            () => Attack.Do(RightAttackSpawner, AttackDistance, () => CreateParticles(), ShakeCamera)
        );
    }

    void CreateParticles()
    {
        var particles = Instantiate(ParticleBehaviour, transform.position + transform.forward * 5f, transform.rotation);
        particles.StartTimer();
    }
}


