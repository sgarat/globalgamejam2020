﻿using System.Linq;
using UnityEngine;

public class ArmsHealth : MonoBehaviour
{
    public GameObject Arm;
    public GameObject ArmMesh;
    public PartsThrower PartsThrower;
    public int MaxHealth;
    public ParticleController sparks;
    int health;

    void Awake()
    {
        health = MaxHealth;
    }

    public void Damage(int amount) => health -= amount;

    void Update()
    {
        if (health <= 0 && IsActive)
        {
            Arm.SetActive(false);
            ArmMesh.SetActive(false);
            sparks.PlayParticles();
            PartsThrower.ThrowArm(gameObject.transform.position, ArmMesh.GetComponentsInChildren<MeshRenderer>().First().material);
        }
    }

    public void Repair(Material floorArm)
    {
        health = MaxHealth;
        Arm.SetActive(true);
        ArmMesh.SetActive(true);
        sparks.StopParticles();
        foreach (var mesh in ArmMesh.GetComponentsInChildren<MeshRenderer>())
        {
            mesh.material = floorArm;
        }
    }

    public int CurrentHealth => health;

    public int InitialHealth => MaxHealth;

    public bool IsActive => Arm.activeSelf;
}