﻿using UnityEngine;

public class HeadExplode : MonoBehaviour
{
    public AnimationCurve VerticalArc;
    public float CurveMultiplier;
    public float HeadThrowSpeed;
    Vector3 finalPosition;
    Vector3 startPosition;
    float lerp = 1;
    
    public void Move(Vector3 finalPosition)
    {
        startPosition = gameObject.transform.position;
        this.finalPosition = finalPosition;
        lerp = 0;
    }
    
    void Update()
    {
        if (lerp < 1)
        {
            lerp += Time.deltaTime * HeadThrowSpeed;
            var positionY = VerticalArc.Evaluate(lerp) * CurveMultiplier;
            var positionX = Mathf.Lerp(startPosition.x, finalPosition.x, lerp);
            var positionZ = Mathf.Lerp(startPosition.z, finalPosition.z, lerp);
            
            gameObject.transform.position = new Vector3(positionX ,positionY, positionZ);
            if(lerp >= 1)
                SoundManager.Instance.PlayScrapFall();
        }
        else
            gameObject.GetComponent<Collider>().enabled = true;
    }
}
