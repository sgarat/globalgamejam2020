using UnityEngine;

public static class KnockBack
{
    public static void Do(AgentMovement agent, Vector3 direction)
    {
        if(agent != null)
            agent.AddForce(direction.normalized);
    }
}