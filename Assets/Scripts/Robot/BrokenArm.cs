using UnityEngine;

public class BrokenArm : MonoBehaviour
{
    public GameObject hand;
    public GameObject arm;
    public ParticleController particle;
    public float ArmThrowSpeed;
    public AnimationCurve VerticalArc;
    public float CurveMultiplier;
    Vector3 finalPosition;
    Vector3 startPosition;
    float lerp = 1;
    

    public void Move(Vector3 finalPosition)
    {
        startPosition = gameObject.transform.position;
        this.finalPosition = finalPosition;
        lerp = 0;
    }

    void Update()
    {
        if (lerp < 1)
        {
            lerp += Time.deltaTime * ArmThrowSpeed;
            var positionY = VerticalArc.Evaluate(lerp) * CurveMultiplier;
            var positionX = Mathf.Lerp(startPosition.x, finalPosition.x, lerp);
            var positionZ = Mathf.Lerp(startPosition.z, finalPosition.z, lerp);
            
            gameObject.transform.position = new Vector3(positionX ,positionY, positionZ);

            if (lerp >= 1)
            {
                SoundManager.Instance.PlayScrapFall();
                particle.PlayParticles();
                
            }
        }
        else
            gameObject.GetComponent<Collider>().enabled = true;
    }

    void OnTriggerEnter(Collider collider)
    {
        var player = collider.GetComponentInParent<RobotController>();
        if (player != null)
        {
            if (!player.LeftArmHealth.IsActive)
            {
                SoundManager.Instance.PlayPickUpSfx();
                player.LeftArmHealth.Repair(hand.GetComponent<MeshRenderer>().material);
                Destroy(gameObject);
            }
            else if (!player.RightArmHealth.IsActive)
            {
                SoundManager.Instance.PlayPickUpSfx();
                player.RightArmHealth.Repair(hand.GetComponent<MeshRenderer>().material);
                Destroy(gameObject);
            }
        }
    }

    public void SetSkin(Material armSkin)
    {
        arm.GetComponent<MeshRenderer>().material = armSkin;
        hand.GetComponent<MeshRenderer>().material = armSkin;
    }
}