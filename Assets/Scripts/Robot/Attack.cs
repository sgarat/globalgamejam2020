using System;
using DefaultNamespace;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Attack
{
    public static void Do(Transform spawner, float distance, Action createParticles, Shake shakeCamera)
    {
        var go = Collisions.GetObject(
            spawner.position, 
            spawner.forward, 
            spawner.transform.right,
            distance
        );

        if (go != null)
        {
            shakeCamera.ShakeCamera();
            var random = Random.Range(0,100);
            
            if(random < 25)
                SoundManager.Instance.PlayVoices();
                
            SoundManager.Instance.PlayHitSfx();
            go.GetComponentInParent<ArmsHealth>()?.Damage(1);
            go.GetComponent<BodyHealth>()?.Damage(1);

            KnockBack.Do(go.GetComponentInParent<AgentMovement>(), spawner.forward);
            createParticles();
        }
    }
}