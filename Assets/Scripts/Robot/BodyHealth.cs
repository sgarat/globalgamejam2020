using UnityEngine;
using UnityEngine.SceneManagement;

public class BodyHealth : MonoBehaviour
{
    public int MaxHealth;
    public DelayerUnity delayer;
    public DelayerUnity endGameTimer;
    public PartsThrower PartsThrower;
    public GameObject Head;
    public Animator Anim;

    int health;
    bool IsDead;

    void Awake()
    {
        health = MaxHealth;
    }

    void Update()
    {
        if (health <= 0 && !IsDead)
        {
            IsDead = true;
            delayer.Delay(4.16f, 1.2f, () => { Anim.SetTrigger("Die"); }, () =>
            {
                SoundManager.Instance.PlayDecapitation();
                ExplodeHead();
            });
            endGameTimer.Delay(8f, 0, () => { }, () => GameStatus.Death.Invoke());
        }
    }

    public void Damage(int amount) => health -= amount;

    public bool IsAlive => health > 0;

    public int CurrentHealth => health;

    public int InitialHealth => MaxHealth;

    void ExplodeHead()
    {
        Head.SetActive(false);
        PartsThrower.ExplodeHead(transform.position);
    }
}