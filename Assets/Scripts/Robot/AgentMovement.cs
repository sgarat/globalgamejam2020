using UnityEngine;
using UnityEngine.AI;

public class AgentMovement : MonoBehaviour
{
    public float Drag;
    public float Force;
    public NavMeshAgent agent;
    Vector3 fixedVelocity;
    Vector3 externalVelocity;

    public void Move(Vector3 velocity)
    {
        fixedVelocity = velocity;
    }

    public void AddForce(Vector3 direction)
    {
        externalVelocity += direction * Force;
    }

    void Update()
    {
        externalVelocity *= Drag;
        agent.Move(fixedVelocity + externalVelocity);
    }
}