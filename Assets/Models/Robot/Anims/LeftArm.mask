%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: LeftArm
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 0
  - m_Path: metarig/body
    m_Weight: 0
  - m_Path: metarig/body/Body
    m_Weight: 0
  - m_Path: metarig/body/head
    m_Weight: 0
  - m_Path: metarig/body/head/Head
    m_Weight: 0
  - m_Path: metarig/body/shoulder.L
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/ShoulderL
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/upper_arm.L
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/upper_arm.L/ArmL
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/upper_arm.L/forearm.L/ForeArmL
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: metarig/body/shoulder.L/upper_arm.L/forearm.L/hand.L/HandL
    m_Weight: 1
  - m_Path: metarig/body/shoulder.R
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/ShoulderR
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/upper_arm.R
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/upper_arm.R/ArmR
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/upper_arm.R/forearm.R/ForeArmR
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 0
  - m_Path: metarig/body/shoulder.R/upper_arm.R/forearm.R/hand.R/HandR
    m_Weight: 0
